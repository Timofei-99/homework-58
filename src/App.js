import './App.css';
import Modal from "./Components/UI/Modal/Modal";
import {useState} from "react";
import SingleButtons from "./Components/SingleButtons/SingleButtons";
import Alert from "./Components/UI/Alert/Alert";

function App() {
    const [modal, setModal] = useState(false);
    const [myAlert, setMyAlert] = useState(false);
    const [btnAlert, setBtnAlert] = useState(false);



    const showModal = () => {
        setModal(true);
    };

    const closeModal = () => {
        setModal(false);
    }

    const showAlert = () => {
        setMyAlert(true);
    }

    const closeAlert = () => {
        setMyAlert(false);
    }


    const openBtnAlert = () => {
        setBtnAlert(true);
    }

    const closeBtnAlert = () => {
        setBtnAlert(false)
    }



  return (
    <div className="App">
        <SingleButtons
            show={showModal}
            open={showAlert}
            view={openBtnAlert}
        />
        <Modal
            title='Some kinda modal title'
            show={modal}
            close={closeModal}
            closed={closeModal}
            continued={() => alert('Hello!')}
        />
        <Alert
            text='This is a warning type alert!'
            show={myAlert}
            type='warning'
            close={closeAlert}
        />
        <Alert
            text='This is a warning type alert!'
            show={btnAlert}
            type='success'
            dismiss={closeBtnAlert}
        />
    </div>
  );
}

export default App;
