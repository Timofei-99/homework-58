import React from 'react';
import '../SingleButtons/SingleButtons.css';

const SingleButtons = props => {
    return (
        <>
        <button className='modalBtn' onClick={props.show}>Open Modal</button>
        <button className='alertBtn' onClick={props.open}>Alert</button>
            <button className='justAlert' onClick={props.view}>Alert btn</button>
        </>
    );
};

export default SingleButtons;