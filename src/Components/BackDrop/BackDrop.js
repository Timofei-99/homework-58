import React from 'react';
import './BackDrop.css';

const BackDrop = ({show}) => {
    return show ? <div className='Backdrop'/> : null;
};

export default BackDrop;