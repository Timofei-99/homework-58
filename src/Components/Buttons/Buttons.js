import React from 'react';
import '../Buttons/Buttons.css';

const Buttons = props => {
    return (
        <button className={['myBtn', props.type].join(' ')} onClick={props.clicked}>
            {props.label}
        </button>
    );
};

export default Buttons;