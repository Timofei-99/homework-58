import React from 'react';
import './Modal.css';
import BackDrop from "../../BackDrop/BackDrop";
import Buttons from "../../Buttons/Buttons";

const Modal = props => {
    const btns = [
        {type: 'primary', label: 'Continue', clicked: props.continued},
        {type: 'danger', label: 'Close', clicked: props.closed}
    ]

    const myBtns = (
        btns.map((p, i) => {
                return (
                    <Buttons
                        key={i}
                        type={p.type}
                        label={p.label}
                        clicked={p.clicked}
                    />
                )
            })
    );

    return (
        <>
            <BackDrop show={props.show}/>
        <div
            className='Modal'
            style={{
                transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
                opacity: props.show ? '1' : '0',
            }}
        >
            <div className='head'>
                <h2>{props.title}</h2>
                <button onClick={props.close}>x</button>
            </div>
            <div className='content'>
                <p>This is modal content</p>
            </div>
            {myBtns}
        </div>
        </>
    );
};

export default Modal;