import React from 'react';
import '../Alert/Alert.css';

const Alert = props => {
    let btn;

    if (props.dismiss !== undefined){
        btn = <button onClick={props.dismiss}>x</button>
    }

    return (
        <div
            className={['Alert', props.type].join(' ')}
            onClick={props.close}
            style={{
                transform: props.show ? 'translateY(-29vh)' : 'translateY(-100vh)',
                opacity: props.show ? '1' : '0',
            }}
        >
            <p>{props.text}</p>
            {btn}
        </div>
    );
};

export default Alert;